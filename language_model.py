from math import log


# Generate P(W)

def filter_unknown(word_set, data):
    for i, e in enumerate(data):
        if e not in word_set:
            data[i] = "<unk>"
    return data


def count_n_grams(line_list, n, n_gram_dict):
    for i in range(0, len(line_list) - n + 1):
        n_gram = tuple(line_list[i:i + n])
        if n_gram not in n_gram_dict:
            n_gram_dict[n_gram] = 0
        n_gram_dict[n_gram] += 1


def remove_unique(n_grams):
    return {key: val for key, val in n_grams.items() if val != 1}


def compute_probs_uni(n_grams, additive_constant=0):
    n_gram_probs = dict()
    all_word_count = sum(n_grams.values())
    for key, val in n_grams.items():
        n_gram_probs[key] = log((val + additive_constant) / (all_word_count + all_word_count * additive_constant), 10)
    return n_gram_probs


def compute_probs(n_grams, history, num_lines, num_words, additive_constant=0):
    n_gram_probs = dict()
    for key, val in n_grams.items():
        print(key)
        if key is ('<s>', 'dobrý', 'den'):
            print(log((val + additive_constant) / (history[key[0:-1]] + additive_constant * num_words), 10))
        try:
            n_gram_probs[key] = log((val + additive_constant) / (history[key[0:-1]] + additive_constant * num_words),
                                    10)
        except Exception as ex:
            n_gram_probs[key] = log((val + additive_constant) / (num_lines + additive_constant * num_words), 10)

    return n_gram_probs


def save_to_file(n_gram_lists, file_name):
    with open(file_name, 'w') as f:
        f.write("\data\\\n")
        for n_grams in n_gram_lists:
            n = len(list(n_grams.keys())[0])
            f.write(f"ngram {n}={len(n_grams)}\n")
        f.write("\n")

        for n_grams in n_gram_lists:
            n = len(list(n_grams.keys())[0])
            f.write(f"\\{n}-grams\n")
            for key, val in n_grams.items():
                f.write(f"{round(val, 6)} {' '.join(key)}\n")
            f.write("\n")

        f.write("\n\end\\")


with open('data_lan/cestina', 'r', encoding="cp1250") as f:
    cestina = set(line.strip() for line in f)

unigrams = dict()
bigrams = dict()
trigrams = dict()

number_of_lines = 0

with open('data_lan/train.txt', 'r', encoding="cp1250") as f:
    for line in f:
        processed_line = filter_unknown(cestina, line.split())
        processed_line.append("</s>")
        count_n_grams(processed_line, 1, unigrams)

        processed_line.insert(0, "<s>")
        count_n_grams(processed_line, 2, bigrams)
        count_n_grams(processed_line, 3, trigrams)

        number_of_lines += 1

trigrams = remove_unique(trigrams)

uni_probs = compute_probs_uni(unigrams)
bi_probs = compute_probs(bigrams, unigrams, number_of_lines, len(unigrams), 1)
tri_probs = compute_probs(trigrams, bigrams, number_of_lines, len(unigrams), 1)

save_to_file([uni_probs, bi_probs, tri_probs], 'lan_mod_output/language_model.arpa')
