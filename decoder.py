from math import log, inf
import copy

# Find argmax_{W} P(W)*P(O|W)

leaves_dic = {}
vocab_trans = [['#']]

# Acoustic model vars
vocab_first = ["#"]
model = {}

beta = 10
gama = 0.5

with open('data_dec/leaves.txt', 'r') as f:
    for t, line in enumerate(f):
        line_s = line.split()
        leaves_dic[line_s[0]] = (log(float(line_s[1]), 10), log(float(line_s[2]), 10), t)

with open('data_dec/vocab', 'r', encoding="cp1250") as f:
    for line in f:
        line_s = line.strip().split('\t')
        transcription = line_s[-1].split(' ')
        vocab_first.append(line_s[0])
        vocab_trans.append(transcription)

with open('lan_mod_output/language_model.arpa', 'r') as f:
    start_parsing = False
    for line in f:
        if start_parsing:
            line_s = line.split()
            if len(line_s) == 0:
                break
            model[line_s[1]] = float(line_s[0])

        if line.strip().endswith("1-grams"):
            start_parsing = True


def get_last_min():
    prob_min = inf
    prob_min_i = -1
    for word_end_i, word_end in enumerate(vocab_trans):
        last_phoneme = word_end[-1]
        prob_last = phi_last[word_end_i][-1] - leaves_dic[last_phoneme][1]

        if prob_last < prob_min:
            prob_min = prob_last
            prob_min_i = word_end_i

    return prob_min, prob_min_i


def get_word_price(word_i):
    if vocab_first[word_i] is "#":
        return 0
    return - beta * model[vocab_first[word_i]] - log(gama, 10)


def get_initial_token_structures():
    token_structure = copy.deepcopy(vocab_trans)
    for word in token_structure:
        for phoneme_i in range(len(word)):
            word[phoneme_i] = 0
    return token_structure, copy.deepcopy(token_structure)


(phi_cur, phi_last) = [copy.deepcopy(vocab_trans), copy.deepcopy(vocab_trans)]

(tok_cur, tok_last) = get_initial_token_structures()
token_table = [("#", None)]

cur_i = 0
with open('data_dec/00170005_14.txt', 'r') as f:
    for t, line in enumerate(f):
        # print(f"Time step {t}")

        (phi_cur, phi_last) = (phi_last, phi_cur)
        (tok_cur, tok_last) = (tok_last, tok_cur)

        line_beta = [-log(float(b), 10) for b in line.split()]
        if t == 0:
            # Initialization
            for word_i, word in enumerate(phi_cur):
                word[0] = line_beta[leaves_dic[word[0]][2]] + get_word_price(word_i)
                for phoneme_i in range(1, len(word)):
                    word[phoneme_i] = inf
        else:
            prob_min_last, prob_min_i_last = get_last_min()
            # Add token to table
            token_table.append((vocab_first[prob_min_i_last], tok_last[prob_min_i_last][-1]))
            for word_i, word in enumerate(vocab_trans):
                for phoneme_i, phoneme in enumerate(word):
                    # Compare the last
                    if phoneme_i == 0:
                        prob_min = phi_last[word_i][phoneme_i] - leaves_dic[phoneme][0]
                        prob_min_last_weighted = prob_min_last + get_word_price(word_i)

                        if prob_min_last_weighted < prob_min:
                            prob_min = prob_min_last_weighted
                            # Token passing
                            tok_cur[word_i][phoneme_i] = t
                    # Compare the 2
                    else:
                        prob_min = phi_last[word_i][phoneme_i] - leaves_dic[phoneme][0]
                        prob_prev = phi_last[word_i][phoneme_i - 1] - leaves_dic[word[phoneme_i - 1]][1]

                        if prob_prev < prob_min:
                            prob_min = prob_prev
                            # Token passing
                            tok_cur[word_i][phoneme_i] = tok_last[word_i][phoneme_i - 1]

                    phi_cur[word_i][phoneme_i] = prob_min + line_beta[leaves_dic[phoneme][2]]

win_prob, win_prob_i = inf, -1
for word_i, word in enumerate(vocab_trans):
    last_phoneme = word[-1]
    prob = phi_cur[word_i][-1] - leaves_dic[last_phoneme][1]
    if prob < win_prob:
        win_prob = prob
        win_prob_i = word_i

print(f"Maximum probability of all the last phonemes: {win_prob}")

# Print the path
cur_tok = tok_cur[win_prob_i][-1]

path = []
while cur_tok != 0:
    path.append(token_table[cur_tok][0])
    cur_tok = token_table[cur_tok][1]

path = path[::-1]
print(f"Path: {path}")
